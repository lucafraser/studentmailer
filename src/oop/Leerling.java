package oop;

public class Leerling {
	private String voornaam, achternaam, email, klascode;

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getKlascode() {
		return klascode;
	}

	public void setKlascode(String klascode) {
		this.klascode = klascode;
	}
	
	public String getFullName() {
		return this.voornaam + " " + this.achternaam;
	}
	
	public Boolean isFullName(String fullName) {
		return fullName.equals(getFullName());
	}
}
