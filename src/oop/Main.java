package oop;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Main extends JPanel {
	private static final long serialVersionUID = 1L;

	public Main() {
		ArrayList<Leerling> leerlingen = new ArrayList<Leerling>();

		JButton send = new JButton("Send");

		DefaultListModel<String> lijstModel = new DefaultListModel<String>();
		JList<String> leerlinglijst = new JList<String>(lijstModel);
		JScrollPane pane = new JScrollPane(leerlinglijst);

		DefaultComboBoxModel<String> klasModel = new DefaultComboBoxModel<String>();
		JComboBox<String> klaslijst = new JComboBox<String>(klasModel);

		add(klaslijst);
		add(pane);
		add(send);

		try {
			URL url = new URL("http://gerdonabbink.nl/luca/Java.txt");
			Scanner s = new Scanner(url.openStream());

			String line = "";
			while (s.hasNextLine()) {
				line = s.nextLine();
				String[] splitted = line.split(",");
				if (splitted.length == 4) {
					Leerling leerling = new Leerling();
					leerling.setVoornaam(splitted[0]);
					leerling.setAchternaam(splitted[1]);
					leerling.setEmail(splitted[2]);
					leerling.setKlascode(splitted[3]);
					leerlingen.add(leerling);
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		for (Leerling leerling : leerlingen) {
			if (klasModel.getIndexOf(leerling.getKlascode()) == -1) {
				klasModel.addElement(leerling.getKlascode());
			}

			if (leerling.getKlascode().equals(klaslijst.getSelectedItem())) {
				lijstModel.addElement(leerling.getFullName());
			}
		}

		klaslijst.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lijstModel.clear();
				for (Leerling leerling : leerlingen) {
					if (leerling.getKlascode().equals(klaslijst.getSelectedItem())) {
						lijstModel.addElement(leerling.getFullName());
					}
				}
			}
		});

		send.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String emails = "";
				for (int i = 0; i < leerlinglijst.getSelectedIndices().length; i++) {
					
					String name = lijstModel.getElementAt(leerlinglijst.getSelectedIndices()[i]);
					System.out.println(name);
					
					for (Leerling leerling : leerlingen) {
						if (leerling.isFullName(name)) {
							emails += leerling.getEmail() + ";";
						}
					}
					emails = emails.substring(0, emails.length()-1);

				    try {
			            Desktop.getDesktop().mail(new URI("mailto:" + emails));
			        } catch (Exception e1) {
			            System.out.println("Kon het mailprogramma niet openen.");
			        }
			    }
				
				emails = emails.trim();
				System.out.println(emails);
			}
		});
	}
}
